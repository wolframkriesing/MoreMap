# MoreMap

Extending JavaScript's `Map` class with useful new functions.

## Develop

- `yarn test` to run the tests
- `yarn prettify` to align the formatting of the source code
import test from "node:test";
import { strict as assert } from "assert";
import { MoreMap } from "./MoreMap.js";

test("MoreMap provides a `getBefore(key)` method", async (t) => {
  await t.test("WHEN calling `getBefore(key)`", async (t) => {
    await t.test("on an empty map THEN return undefined (just like `get()`)", () => {
      const map = new MoreMap();
      assert.equal(map.getBefore("irrelevant key"), undefined);
    });

    await t.test("on a map with one element THEN return undefined", () => {
      const map = new MoreMap([["one", 1]]);
      assert.equal(map.getBefore("one"), undefined);
    });

    await t.test("AND there is one item before THEN return the item before", () => {
      const map = new MoreMap([
        ["zero", 0],
        ["one", 1],
        ["two", 2],
      ]);
      assert.equal(map.getBefore("two"), 1);
    });
  });
});

test("MoreMap provides a `getAfter(key)` method", async (t) => {
  await t.test("WHEN calling `getAfter(key)`", async (t) => {
    await t.test("on an empty map THEN return undefined", () => {
      const map = new MoreMap();
      assert.equal(map.getAfter("irrelevant key"), undefined);
    });

    await t.test("on a map with one element THEN return undefined", () => {
      const map = new MoreMap([["one", 1]]);
      assert.equal(map.getAfter("one"), undefined);
    });

    await t.test("AND there is one after THEN return the item after", () => {
      const map = new MoreMap([
        ["zero", 0],
        ["one", 1],
        ["two", 2],
      ]);
      assert.equal(map.getAfter("one"), 2);
    });
  });
});

export class MoreMap extends Map {
  getBefore(keyIn) {
    let lastKey;
    for (const key of this.keys()) {
      if (key === keyIn) return this.get(lastKey);
      lastKey = key;
    }
    return undefined;
  }

  getAfter(keyIn) {
    let returnNext = false;
    for (const key of this.keys()) {
      if (returnNext) return this.get(key);
      if (key === keyIn) returnNext = true;
    }
    return undefined;
  }
}
